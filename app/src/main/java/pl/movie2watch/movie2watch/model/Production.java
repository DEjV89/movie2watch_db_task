package pl.movie2watch.movie2watch.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dkrezel on 2015-05-30.
 */
@DatabaseTable(tableName = "production")
public class Production implements Comparable<Production> {

	@DatabaseField(generatedId = true, columnName = "id")
	private int mID;

	@DatabaseField(columnName = "name")
	private String mName;

	@ForeignCollectionField()
	private ForeignCollection<MovieProduction> mMovieList;

	public Production() {
	}

	public Production(String mName) {
		this.mName = mName;
	}

	public int getID() {
		return mID;
	}

	public void setID(int mID) {
		this.mID = mID;
	}

	public String getName() {
		return mName;
	}

	public void setName(String mName) {
		this.mName = mName;
	}

	public ForeignCollection<MovieProduction> getMovieList() {
		return mMovieList;
	}

	public void setMovieList(ForeignCollection<MovieProduction> mMovieList) {
		this.mMovieList = mMovieList;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Production that = (Production) o;

		if (mID != that.mID) return false;
		if (!mName.equals(that.mName)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = mID;
		result = 31 * result + mName.hashCode();
		return result;
	}

	@Override
	public String toString() {
		return this.mName;
	}

	@Override
	public int compareTo(Production another) {
		return this.mName.compareTo(another.mName);
	}
}
