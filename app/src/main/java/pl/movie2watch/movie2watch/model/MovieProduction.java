package pl.movie2watch.movie2watch.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dkrezel on 2015-05-31.
 */
@DatabaseTable(tableName = "movie_production")
public class MovieProduction {

	@DatabaseField(generatedId = true, columnName = "id")
	private int mID;

	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true)
	private Movie mMovie;

	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true)
	private Production mProduction;

	public int getID() {
		return mID;
	}

	public void setID(int mID) {
		this.mID = mID;
	}

	public Movie getMovie() {
		return mMovie;
	}

	public void setMovie(Movie mMovie) {
		this.mMovie = mMovie;
	}

	public Production getProduction() {
		return mProduction;
	}

	public void setProduction(Production mProduction) {
		this.mProduction = mProduction;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		MovieProduction that = (MovieProduction) o;

		if (mID != that.mID) return false;
		if (!mMovie.equals(that.mMovie)) return false;
		if (!mProduction.equals(that.mProduction)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = mID;
		result = 31 * result + mMovie.hashCode();
		result = 31 * result + mProduction.hashCode();
		return result;
	}
}
