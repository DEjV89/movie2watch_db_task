package pl.movie2watch.movie2watch.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by dkrezel on 2015-05-30.
 */
@DatabaseTable(tableName = "movie")
public class Movie {

	@DatabaseField(generatedId = true, columnName = "id")
	private int mID;

	@DatabaseField(columnName = "name")
	private String mName;

	@DatabaseField(columnName = "production_date")
	private Date mProductionDate;

	@DatabaseField(columnName = "reminder_date")
	private Date mReminderDate;

	@ForeignCollectionField(eager = true)
	private ForeignCollection<MovieKind> mKindList;

	@ForeignCollectionField(eager = true)
	private ForeignCollection<MovieProduction> mProductionList;

	public Movie() {
	}

	public Movie(String mName, Date mProductionDate, Date mReminderDate) {
		this.mName = mName;
		this.mProductionDate = mProductionDate;
		this.mReminderDate = mReminderDate;
	}

	public int getID() {
		return mID;
	}

	public void setID(int mID) {
		this.mID = mID;
	}

	public String getName() {
		return mName;
	}

	public void setName(String mName) {
		this.mName = mName;
	}

	public Date getProductionDate() {
		return mProductionDate;
	}

	public void setProductionDate(Date mProductionDate) {
		this.mProductionDate = mProductionDate;
	}

	public Date getReminderDate() {
		return mReminderDate;
	}

	public void setReminderDate(Date mReminderDate) {
		this.mReminderDate = mReminderDate;
	}

	public ForeignCollection<MovieKind> getKindList() {
		return mKindList;
	}

	public void setKindList(ForeignCollection<MovieKind> mKindList) {
		this.mKindList = mKindList;
	}

	public ForeignCollection<MovieProduction> getProductionList() {
		return mProductionList;
	}

	public void setProductionList(ForeignCollection<MovieProduction> mProductionList) {
		this.mProductionList = mProductionList;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Movie movie = (Movie) o;

		if (mID != movie.mID) return false;
		if (!mName.equals(movie.mName)) return false;
		if (!mProductionDate.equals(movie.mProductionDate)) return false;
		if (mReminderDate != null ? !mReminderDate.equals(movie.mReminderDate) : movie.mReminderDate != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = mID;
		result = 31 * result + mName.hashCode();
		result = 31 * result + mProductionDate.hashCode();
		result = 31 * result + (mReminderDate != null ? mReminderDate.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Movie ID : ");
		stringBuilder.append(mID);
		stringBuilder.append("\n");
		stringBuilder.append("Movie name : ");
		stringBuilder.append(mName);
		stringBuilder.append("\n");
		stringBuilder.append("Production date : ");
		stringBuilder.append(mProductionDate.toString());
		return stringBuilder.toString();
	}
}
