package pl.movie2watch.movie2watch.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dkrezel on 2015-05-31.
 */
@DatabaseTable(tableName = "movie_kind")
public class MovieKind {

	@DatabaseField(generatedId = true, columnName = "id")
	private int mID;

	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true)
	private Movie mMovie;

	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true)
	private Kind mKind;

	public int getID() {
		return mID;
	}

	public void setID(int mID) {
		this.mID = mID;
	}

	public Movie getMovie() {
		return mMovie;
	}

	public void setMovie(Movie mMovie) {
		this.mMovie = mMovie;
	}

	public Kind getKind() {
		return mKind;
	}

	public void setKind(Kind mKind) {
		this.mKind = mKind;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		MovieKind movieKind = (MovieKind) o;

		if (mID != movieKind.mID) return false;
		if (!mKind.equals(movieKind.mKind)) return false;
		if (!mMovie.equals(movieKind.mMovie)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = mID;
		result = 31 * result + mMovie.hashCode();
		result = 31 * result + mKind.hashCode();
		return result;
	}
}
