package pl.movie2watch.movie2watch.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import pl.movie2watch.movie2watch.R;
import pl.movie2watch.movie2watch.model.Movie;
import pl.movie2watch.movie2watch.model.MovieKind;
import pl.movie2watch.movie2watch.model.MovieProduction;

/**
 * Created by dkrezel on 2015-05-31.
 */
public class MovieAdapter extends ArrayAdapter<Movie> {

	private LayoutInflater mInflater;
	private Map<Integer, Boolean> mSelectedMovieMap;
	private List<Movie> mMovieList;
	private List<Movie> mMovieList4Search;

	public MovieAdapter(Context context, List<Movie> data) {
		super(context, R.layout.movie_item, data);
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mSelectedMovieMap = new HashMap<>(data.size());
		mMovieList4Search = data;
		mMovieList = new ArrayList<>(data.size());
		mMovieList.addAll(data);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final MovieHolder mHolder;
		final Movie mMovie = getItem(position);
		if (convertView == null) {
			mHolder = new MovieHolder();
			convertView = mInflater.inflate(R.layout.movie_item, parent, false);

			TextView mMovieName = (TextView) convertView.findViewById(R.id.movie_name);
			TextView mMovieKind = (TextView) convertView.findViewById(R.id.movie_kind);
			TextView mMovieProduction = (TextView) convertView.findViewById(R.id.movie_production);
			CheckBox mMovieSelectButton = (CheckBox) convertView.findViewById(R.id.btn_movie_select);

			mHolder.mMovieName = mMovieName;
			mHolder.mMovieKind = mMovieKind;
			mHolder.mMovieProduction = mMovieProduction;
			mHolder.mMovieSelectButton = mMovieSelectButton;

			mHolder.mMovieSelectButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					CheckBox mCheckBox = (CheckBox) v;
					int tag = (int) mCheckBox.getTag();
					mSelectedMovieMap.put(tag, mCheckBox.isChecked());
				}
			});

			convertView.setTag(mHolder);
		} else {
			mHolder = (MovieHolder) convertView.getTag();
		}

		mHolder.mMovieName.setText(mMovie.getName());
		StringBuilder stringBuilder = new StringBuilder();
		for (MovieKind kind : mMovie.getKindList()) {
			stringBuilder.append(kind.getKind().getName());
			stringBuilder.append(", ");
		}
		mHolder.mMovieKind.setText(stringBuilder.toString());

		stringBuilder = new StringBuilder();
		for (MovieProduction production : mMovie.getProductionList()) {
			stringBuilder.append(production.getProduction().getName());
			stringBuilder.append(", ");
		}
		mHolder.mMovieProduction.setText(stringBuilder.toString());

		mHolder.mMovieSelectButton.setTag(position);
		if (mSelectedMovieMap.size() == 0) {
			mHolder.mMovieSelectButton.setChecked(false);
		} else {
			if (mSelectedMovieMap.containsKey(position)) {
				mHolder.mMovieSelectButton.setChecked(mSelectedMovieMap.get(position));
			} else {
				mHolder.mMovieSelectButton.setChecked(false);
			}
		}

		return convertView;
	}

	public Map<Integer, Boolean> getSelectedMovieMap() {
		return mSelectedMovieMap;
	}

	public void filter(String stringFilter) {
		stringFilter = stringFilter.toLowerCase(Locale.getDefault());
		mMovieList4Search.clear();
		if (stringFilter.length() == 0) {
			mMovieList4Search.addAll(mMovieList);
		} else {
			for (Movie movie : mMovieList) {
				if (movie.getName().toLowerCase(Locale.getDefault()).contains(stringFilter)) {
					mMovieList4Search.add(movie);
				}
			}
			mSelectedMovieMap.clear();
		}
		notifyDataSetChanged();
	}

	public void refreshMovieList(List<Movie> mMovieList) {
		this.mMovieList.clear();
		this.mMovieList.addAll(mMovieList);
	}

	private static class MovieHolder {
		TextView mMovieName;
		TextView mMovieKind;
		TextView mMovieProduction;
		CheckBox mMovieSelectButton;
	}

}
