package pl.movie2watch.movie2watch.preferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by dkrezel on 2015-06-03.
 */
public class UserPreferences {
	private static final String PREFFS_NAME = "user_preferences";

	private static final String KEY_TEXT_SEARCH = "pref.text_search";

	private String mMovieName;

	private SharedPreferences mPreferences;

	public UserPreferences(Context mContext) {
		mPreferences = mContext.getSharedPreferences(PREFFS_NAME, Context.MODE_PRIVATE);

		refresh();
	}

	public UserPreferences refresh() {
		this.mMovieName = mPreferences.getString(KEY_TEXT_SEARCH, "");
		return this;
	}

	public void save() {
		mPreferences.edit()
				.putString(KEY_TEXT_SEARCH, mMovieName)
				.commit();
	}

	public String getMovieName() {
		return mMovieName;
	}

	public UserPreferences setMovieName(String mMovieName) {
		this.mMovieName = mMovieName;
		return this;
	}
}
