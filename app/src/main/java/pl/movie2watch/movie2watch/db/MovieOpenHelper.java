package pl.movie2watch.movie2watch.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import pl.movie2watch.movie2watch.model.Kind;
import pl.movie2watch.movie2watch.model.Movie;
import pl.movie2watch.movie2watch.model.MovieKind;
import pl.movie2watch.movie2watch.model.MovieProduction;
import pl.movie2watch.movie2watch.model.Production;

/**
 * Created by dkrezel on 2015-05-30.
 */
public class MovieOpenHelper extends OrmLiteSqliteOpenHelper {

	private static final int DB_VERSION = 1;
	public static final String DB_NAME = "movie.db";

	private Dao<Kind, Integer> mKindDao;
	private Dao<Production, Integer> mProductionDao;

	public MovieOpenHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	// Wywoływana kiedy baza danych nie istnieje
	// Utworzenie struktury bazy danych, nadanie wartości początkowych
	public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {

		// Utworzenie tabel w bazie danych
		try {
			TableUtils.createTable(connectionSource, Movie.class);
			TableUtils.createTable(connectionSource, Kind.class);
			TableUtils.createTable(connectionSource, Production.class);
			TableUtils.createTable(connectionSource, MovieKind.class);
			TableUtils.createTable(connectionSource, MovieProduction.class);

			// Utworzenie obiektu dostępu do danych
			mKindDao = DaoManager.createDao(connectionSource, Kind.class);
			mProductionDao = DaoManager.createDao(connectionSource, Production.class);

			insertKindValues();
			insertProductionValues();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	// Implementacja zmiany struktury z wersji na wersje
	public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
	}

	private void insertKindValues() throws SQLException {
		mKindDao.create(new Kind("akcja"));
		mKindDao.create(new Kind("dokumentalny"));
		mKindDao.create(new Kind("dramat"));
		mKindDao.create(new Kind("horror"));
		mKindDao.create(new Kind("western"));
		mKindDao.create(new Kind("komedia"));
		mKindDao.create(new Kind("przygodowy"));
		mKindDao.create(new Kind("sci-fi"));
		mKindDao.create(new Kind("thriller"));
	}

	private void insertProductionValues() throws SQLException {
		mProductionDao.create(new Production("Austria"));
		mProductionDao.create(new Production("Belgia"));
		mProductionDao.create(new Production("Chiny"));
		mProductionDao.create(new Production("Czechy"));
		mProductionDao.create(new Production("Francja"));
		mProductionDao.create(new Production("Hiszpania"));
		mProductionDao.create(new Production("Niemcy"));
		mProductionDao.create(new Production("Polska"));
		mProductionDao.create(new Production("Portugalia"));
		mProductionDao.create(new Production("Rosja"));
		mProductionDao.create(new Production("Szwecja"));
		mProductionDao.create(new Production("USA"));
		mProductionDao.create(new Production("Wielka Brytania"));
		mProductionDao.create(new Production("Włochy"));
	}
}
