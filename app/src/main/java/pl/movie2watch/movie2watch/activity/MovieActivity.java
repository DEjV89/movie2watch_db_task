package pl.movie2watch.movie2watch.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import pl.movie2watch.movie2watch.R;
import pl.movie2watch.movie2watch.adapter.KindAdapter;
import pl.movie2watch.movie2watch.adapter.ProductionAdapter;
import pl.movie2watch.movie2watch.db.MovieOpenHelper;
import pl.movie2watch.movie2watch.model.Kind;
import pl.movie2watch.movie2watch.model.Movie;
import pl.movie2watch.movie2watch.model.MovieKind;
import pl.movie2watch.movie2watch.model.MovieProduction;
import pl.movie2watch.movie2watch.model.Production;

/**
 * Created by dkrezel on 2015-05-30.
 */
public class MovieActivity extends ActionBarActivity {

	@InjectView(R.id.movie_name)
	protected TextView mTextView;

	@InjectView(R.id.movie_kind)
	protected Spinner mKindSpinner;

	@InjectView(R.id.movie_production)
	protected Spinner mProductionSpinner;

	@InjectView(R.id.btn_save_id)
	protected Button mSaveButton;

	private ConnectionSource mConnection;
	private Dao<Kind, Integer> mKindDao;
	private Dao<Production, Integer> mProductionDao;
	private Dao<Movie, Integer> mMovieDao;
	private Dao<MovieKind, Integer> mMovieKindDao;
	private Dao<MovieProduction, Integer> mMovieProductionDao;

	private Movie mMovie;
	private Kind mKind;
	private Production mProduction;
	int[] movieKindIdTab;
	int[] movieProductionIdTab;
	int mSelectedKindId = -1;
	int mSelectedProductionId = -1;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.movie_layout);
		ButterKnife.inject(this);

		mConnection = new AndroidConnectionSource(new MovieOpenHelper(this));

		mMovie = new Movie();

		try {
			mKindDao = DaoManager.createDao(mConnection, Kind.class);
			mProductionDao = DaoManager.createDao(mConnection, Production.class);
			mMovieDao = DaoManager.createDao(mConnection, Movie.class);
			mMovieKindDao = DaoManager.createDao(mConnection, MovieKind.class);
			mMovieProductionDao = DaoManager.createDao(mConnection, MovieProduction.class);

			if (getIntent().hasExtra(MainActivity.SELECTED_ITEM_ID)) {
				int mSelectedMovieId = getIntent().getExtras().getInt(MainActivity.SELECTED_ITEM_ID);
				mMovie = mMovieDao.queryForId(mSelectedMovieId);
				refreshMovieItem();
			} else {
				mMovie = new Movie();
			}

			initiateKindSpinner(mSelectedKindId);
			initiateProductionSpinner(mSelectedProductionId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void refreshMovieItem() {
		int counter = 0;
		mTextView.setText(mMovie.getName());

		movieKindIdTab = new int[mMovie.getKindList().size()];
		for (MovieKind movieKind : mMovie.getKindList()) {
			movieKindIdTab[counter++] = movieKind.getKind().getID();
		}

		counter = 0;
		movieProductionIdTab = new int[mMovie.getProductionList().size()];
		for (MovieProduction movieProduction : mMovie.getProductionList()) {
			movieProductionIdTab[counter++] = movieProduction.getProduction().getID();
		}

		mSelectedKindId = movieKindIdTab[0];
		mSelectedProductionId = movieProductionIdTab[0];
	}

	@OnClick(R.id.btn_save_id)
	public void onSaveButton() {
		mKind = (Kind) mKindSpinner.getSelectedItem();
		mProduction = (Production) mProductionSpinner.getSelectedItem();

		mMovie.setName(mTextView.getText().toString());
		mMovie.setProductionDate(new Date());
		mMovie.setReminderDate(new Date());

		try {
			mMovieDao.createOrUpdate(mMovie);

			// usuń wszystkie poprzednie rekordy
			mMovieKindDao.delete(mMovie.getKindList());

			// Utwórz nowe rekordy
			MovieKind mMovieKind = new MovieKind();
			mMovieKind.setMovie(mMovie);
			mMovieKind.setKind(mKind);

			mMovieKindDao.create(mMovieKind);

			// usuń wszystkie poprzednie rekordy
			mMovieProductionDao.delete(mMovie.getProductionList());

			// Utwórz nowe rekordy
			MovieProduction mMovieProduction = new MovieProduction();
			mMovieProduction.setMovie(mMovie);
			mMovieProduction.setProduction(mProduction);

			mMovieProductionDao.create(mMovieProduction);

			finish();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void initiateKindSpinner(int selectedItemId) throws SQLException {
		List<Kind> kindList = mKindDao.queryForAll();
		Collections.sort(kindList);    // posortuj listę
		KindAdapter mKindAdapter = new KindAdapter(this, kindList);
		mKindSpinner.setAdapter(mKindAdapter);
		if (selectedItemId != -1) {
			for (Kind kind : kindList) {
				if (kind.getID() == selectedItemId) {
					mKindSpinner.setSelection(kindList.indexOf(kind));
				}
			}
		}
	}

	private void initiateProductionSpinner(int selectedItemId) throws SQLException {
		List<Production> productionList = mProductionDao.queryForAll();
		Collections.sort(productionList);
		ProductionAdapter mProductionAdapter = new ProductionAdapter(this, productionList);
		mProductionSpinner.setAdapter(mProductionAdapter);
		if (selectedItemId != -1) {
			for (Production production : productionList) {
				if (production.getID() == selectedItemId) {
					mProductionSpinner.setSelection(productionList.indexOf(production));
				}
			}
		}
	}
}
