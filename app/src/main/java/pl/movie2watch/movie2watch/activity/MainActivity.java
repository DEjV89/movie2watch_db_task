package pl.movie2watch.movie2watch.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;

import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.Locale;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;
import pl.movie2watch.movie2watch.R;
import pl.movie2watch.movie2watch.adapter.MovieAdapter;
import pl.movie2watch.movie2watch.db.MovieOpenHelper;
import pl.movie2watch.movie2watch.model.Movie;
import pl.movie2watch.movie2watch.preferences.UserPreferences;

/**
 * Created by dkrezel on 2015-05-30.
 */
public class MainActivity extends ActionBarActivity {

	public static final String SELECTED_ITEM_ID = "selected.movie.id";

	@InjectView(R.id.movie_list_view)
	protected ListView mMovieListView;

	@InjectView(R.id.search_movie_id)
	protected EditText mSearchMovieName;

	private MovieAdapter mAdapter;

	// Połączenie do DB
	private ConnectionSource mConnection;
	private Dao<Movie, Integer> mMovieDao;

	private UserPreferences mUserPreferences;

	private MovieTextWatcher mMovieTextWatcher;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.movie_main_list);
		ButterKnife.inject(this);
		mMovieTextWatcher = new MovieTextWatcher();

		mConnection = new AndroidConnectionSource(new MovieOpenHelper(this));
		mUserPreferences = new UserPreferences(this);

		if (savedInstanceState == null) {
			readPreferences();
		}

		try {
			mMovieDao = DaoManager.createDao(mConnection, Movie.class);
			mAdapter = new MovieAdapter(this, mMovieDao.queryForAll());
			mMovieListView.setAdapter(mAdapter);
			mSearchMovieName.addTextChangedListener(mMovieTextWatcher);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	protected void readPreferences() {
		mSearchMovieName.setText(mUserPreferences.getMovieName());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (R.id.create_item == item.getItemId()) {
			// Otwórz nową aktywność w celu utworzenia elementu listy
			Intent mIntent = new Intent(this, MovieActivity.class);
			startActivity(mIntent);
		}
		if (R.id.delete_items == item.getItemId()) {
			deleteSelectedItems(mAdapter.getSelectedMovieMap());
		}
		return super.onOptionsItemSelected(item);
	}

	private void deleteSelectedItems(Map<Integer, Boolean> mSelectedMovieMap) {
		if (mSelectedMovieMap.size() > 0) {
			for (Map.Entry<Integer, Boolean> entry : mSelectedMovieMap.entrySet()) {
				if (entry.getValue()) {
					Movie mMovie = mAdapter.getItem(entry.getKey().intValue());
					try {
						mMovieDao.delete(mMovie);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			try {
				// odśwież listę pozycji
				mSelectedMovieMap.clear();
				mAdapter.clear();
				mAdapter.refreshMovieList(mMovieDao.queryForAll());
				mAdapter.addAll(mMovieDao.queryForAll());
				mAdapter.notifyDataSetChanged();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		mAdapter.clear();
		try {
			mAdapter.addAll(mMovieDao.queryForAll());
			mAdapter.refreshMovieList(mMovieDao.queryForAll());
			mMovieTextWatcher.afterTextChanged(null);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		setAndSaveUserPrefs();
	}


	@OnItemClick(R.id.movie_list_view)
	public void onItemClick(int position) {
		Movie mSelectedItem = (Movie) mMovieListView.getItemAtPosition(position);

		Intent mIntent = new Intent(this, MovieActivity.class);
		mIntent.putExtra(SELECTED_ITEM_ID, mSelectedItem.getID());
		startActivity(mIntent);
	}

	private class MovieTextWatcher implements TextWatcher {
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {

		}

		@Override
		public void afterTextChanged(Editable s) {
			String mMovieName = mSearchMovieName.getText().toString().toLowerCase(Locale.getDefault());
			mAdapter.filter(mMovieName);
		}
	}

	private void setAndSaveUserPrefs() {
		mUserPreferences.setMovieName(mSearchMovieName.getText().toString());
		mUserPreferences.save();
	}
}
